<?php

namespace App\Model;

class Project
{
    /**
     * @var array
     */
    public $_data; // Change this to private $_data;

    public function __construct($data)
    {
        $this->_data = $data;
    }

    /**
     * @return int
     */
    public function getId()
    {
        // we don't know 'id' is set or not, so we should check it first
        // if 'id' is required data, we should throw an exception
        // else we should set default value for it
        return (int) $this->_data['id'];
    }

    /**
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->_data);
    }
}
