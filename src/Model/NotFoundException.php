<?php

namespace App\Model;

// Move this class to src/Exceptions/NotFoundException.php
class NotFoundException extends \Exception
{
}
