<?php

// Replace namespace Api\Controller; with namespace App\Controller;
namespace Api\Controller;

use App\Model;
use App\Storage\DataStorage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController
{
    /**
     * @var DataStorage
     */
    private $storage;

    // This class serve for project and task actions,
    // we might need to split it into two classes: ProjectController and TaskController.
    // ProjectController will contain project actions and TaskController will contain task actions.
    // We can consider to create a parent class Controller

    // In case we have different "DataStorage" class and difference Controller class
    // we will pass corresponding "DataStorage" class to corresponding Controller class
    public function __construct(DataStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param Request $request
     *
     * @Route("/project/{id}", name="project", method="GET")
     */
    public function projectAction(Request $request)
    {
        try {
            // parameter in getProjectById should be an integer, so we should make sure that $request->get('id') is an integer
            // we can validate it required and integer type
            $project = $this->storage->getProjectById($request->get('id'));

            return new Response($project->toJson());
        } catch (Model\NotFoundException $e) {
            // like response error in method projectCreateTaskAction, they should be the same format, like array with key 'error'
            // 404 can be replaced by Response::HTTP_NOT_FOUND
            return new Response('Not found', 404);
        } catch (\Throwable $e) {
            // Same as above
            // 500 can be replaced by Response::HTTP_INTERNAL_SERVER_ERROR
            return new Response('Something went wrong', 500);
        }
    }

    /**
     * @param Request $request
     *
     * @Route("/project/{id}/tasks", name="project-tasks", method="GET")
     */
    public function projectTaskPagerAction(Request $request)
    {
        // parameter in getTasksByProjectId should be integer, so we have to validate it first before pass it to getTasksByProjectId
        $tasks = $this->storage->getTasksByProjectId(
            $request->get('id'),
            $request->get('limit'),
            $request->get('offset')
        );

        return new Response(json_encode($tasks));
    }

    /**
     * @param Request $request
     *
     * @Route("/project/{id}/tasks", name="project-create-task", method="PUT")
     */
    // To create new resource, we should use POST method instead of PUT method
    public function projectCreateTaskAction(Request $request)
    {
        //we should validate $request->get('id') first
		$project = $this->storage->getProjectById($request->get('id'));
		if (!$project) {
            // Should pass status code to JsonResponse, using Response::HTTP_NOT_FOUND
			return new JsonResponse(['error' => 'Not found']);
		}

		return new JsonResponse(
            // To create new task, we should pass title, status to createTask method via an array
            // replace $_REQUEST with $request->all(), we should validate $request->all() first, enough 2 keys above
            // or create an array with 2 keys: title, status with values from $request
			$this->storage->createTask($_REQUEST, $project->getId())
		);
    }
}
