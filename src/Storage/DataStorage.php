<?php

namespace App\Storage;

use App\Model;

class DataStorage
{
    /**
     * @var \PDO
     */
    public $pdo; // make it protected instead of public

    // This class contains three methods is getProjectById, getTasksByProjectId, createTask which interact with database
    // and that three methods have different domain logic, so we should separate them into two classes: ProjectStorage and TaskStorage,
    // two classes will have the same property $pdo, so we should create a parent class DataStorage which contains $pdo property
    // all query should replace by prepare statement and execute method because they all contains placeholders, it will prevent SQL injection

    public function __construct()
    {
        // Moving dbname, host, user to .env file
        $this->pdo = new \PDO('mysql:dbname=task_tracker;host=127.0.0.1', 'user');
    }

    /**
     * @param int $projectId
     * @throws Model\NotFoundException
     */
    // add type for $projectId (int $projectId)
    public function getProjectById($projectId)
    {
        // use prepare statement then use execute method to execute a prepared statement
        $stmt = $this->pdo->query('SELECT * FROM project WHERE id = ' . (int) $projectId);

        if ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            return new Model\Project($row);
        }

        throw new Model\NotFoundException();
    }

    /**
     * @param int $project_id
     * @param int $limit
     * @param int $offset
     */
    // add type for $limit and $offset like (int $project_id, int $limit, int $offset),
    // can consider to set default value for $limit and $offset
    public function getTasksByProjectId(int $project_id, $limit, $offset)
    {
        // use prepare statement then use execute method to execute a prepared statement
        $stmt = $this->pdo->query("SELECT * FROM task WHERE project_id = $project_id LIMIT ?, ?");
        $stmt->execute([$limit, $offset]);

        $tasks = [];
        foreach ($stmt->fetchAll() as $row) {
            $tasks[] = new Model\Task($row);
        }

        return $tasks;
    }

    /**
     * @param array $data
     * @param int $projectId
     * @return Model\Task
     */
    // add type for $projectId (array $data, int $projectId)
    public function createTask(array $data, $projectId)
    {
        $data['project_id'] = $projectId;

        $fields = implode(',', array_keys($data));
        $values = implode(',', array_map(function ($v) {
            return is_string($v) ? '"' . $v . '"' : $v;
        }, $data));

        // use prepare statement then use execute method to execute a prepared statement
        $this->pdo->query("INSERT INTO task ($fields) VALUES ($values)");
        // using lastInsertId method to get the last inserted id instead of using query method
        $data['id'] = $this->pdo->query('SELECT MAX(id) FROM task')->fetchColumn();

        // $data is input/payload data, it doesn't contain all data of a task like created_at
        // in case we want to get all data of a task, we should get it from database
        return new Model\Task($data);
    }
}
