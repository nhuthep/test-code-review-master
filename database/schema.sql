CREATE TABLE project (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP
) Engine=InnoDB;

CREATE TABLE task (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    project_id INT NOT NULL,
    title VARCHAR(255) NOT NULL,
    status VARCHAR(16) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP
) Engine=InnoDB;

-- Table name should change to plural form, projects and tasks
-- Set foreign key constraint, project_id on table task references to id on table project
-- status should be enum type or tinyint type
-- consider to add updated_at column
